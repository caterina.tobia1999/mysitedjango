"""mysite URL Configuration"""

from django.urls import path
from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'), # ex: /polls/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # '<int:question_id>/', views.detail, -> ex: /polls/1/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # '<int:question_id>/results/', views.results, -> ex: /polls/1/results/
    path('<int:question_id>/vote/', views.vote, name='vote'), # ex: /polls/1/vote/
]

# remove our old index, detail, and results views and use Django’s generic views : from <question_id> to <pk>.