# Create your models here.
"""
remember the three-step guide to making model changes:
- Change your models (in models.py).
- Run python manage.py makemigrations to create migrations for those changes
- Run python manage.py migrate to apply those changes to the database.
"""
from django.db import models
import datetime
from django.utils import timezone
from django.contrib import admin

# Create a database schema (CREATE TABLE statements) for this app.
class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def __str__(self):
        return self.question_text

    @admin.display(
        boolean=True,
        ordering='pub_date',
        description='Published recently?',
    )

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=2) <= self.pub_date <= now #  it will only return True if the date is also in the past
        # return self.pub_date >= timezone.now() - datetime.timedelta(days=1) # it returns true if pub_date is in the future

# Create a Python database-access API for accessing Question and Choice objects.
class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text

