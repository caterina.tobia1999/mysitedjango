
from django.urls import path, include
from . import views  # "." = "this directory" = events
from django.contrib import admin

# app_name = 'events'
urlpatterns = [
    path('', views.home, name="home"),       # home
    path('home/', views.home, name="home"),       # home

   # path('', views.calendar2, name="calendar2"),
   # path('<int:year>/<str:month>', views.calendar2, name="calendar2"),  # calendario statico

    path('calendar', views.calendar, name="calendar"),
    path('calendar/<int:pYear>/<int:pMonth>', views.calendar, name="calendar"), # calendario dinamico

    path('event/<int:pk>/', views.EventView.as_view(), name='event_detail'), # link a evento dentro calendario
    
    path('events', views.all_events, name="list_events"),
    path('sports', views.all_sports, name="list_sports"),
    path('users', views.all_myUsers, name="list_users"),
    path('venues', views.all_venues, name="list_venues"),

    path('add_venue', views.add_venue, name="add_venue"),
    path('add_myUser', views.add_myUser, name="add_myUser"),
    path('add_sport', views.add_sport, name="add_sport"),
    path('add_event', views.add_event, name="add_event"),

    path('update_venue/<venue_id>', views.update_venue, name="update_venue"),
    path('delete_venue/<venue_id>', views.delete_venue, name="delete_venue"),
    path('update_myUser/<myUser_id>', views.update_myUser, name="update_myUser"),
    path('delete_myUser/<myUser_id>', views.delete_myUser, name="delete_myUser"),
    path('update_sport/<sport_id>', views.update_sport, name="update_sport"),
    path('delete_sport/<sport_id>', views.delete_sport, name="delete_sport"),
    path('update_event/<event_id>', views.update_event, name="update_event"),
    path('delete_event/<event_id>', views.delete_event, name="delete_event"),

    path('search_event', views.search_event, name="search_event"),

]