from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User  # djangoUser -> manager
# for the calendar:
from django.utils.html import conditional_escape as esc
from django.utils.safestring import mark_safe
from itertools import groupby
from calendar import HTMLCalendar, monthrange
from datetime import date
from datetime import datetime
from django.urls import reverse
from django.shortcuts import render, redirect

# Create a database schema (CREATE TABLE statements) for this app.

class Venue(models.Model):
    name = models.CharField('Venue Name', max_length=120)
    city = models.CharField('Venue City', max_length=120)
    address = models.CharField('Venue Address', max_length=300)
    zip_code = models.CharField('Zip Code', max_length=10)
    phone = models.CharField('Venue Contact Phone', max_length=25, blank=True)
    web = models.URLField('Website Address', blank=True)
    email_address = models.EmailField('Venue Email Address', blank=True)

    objects = models.Manager()

    def __str__(self):
        return self.name + ' : ' + self.address


class myUser(models.Model):
    first_name = models.CharField('User First Name', max_length=60)
    last_name = models.CharField('User Last Name', max_length=60)
    email = models.EmailField('User Email Address')
    birth_date = models.DateField('Birth Date')

    objects = models.Manager()

    def __str__(self):
        return self.first_name + ' ' + self.last_name


class Sport(models.Model):
    name = models.CharField('Sport Name', max_length=120)
    description = models.TextField('Description', blank=True)

    objects = models.Manager()

    def __str__(self):
        return self.name


class Event(models.Model):
    name = models.CharField('Event Name', max_length=120)
    start_date = models.DateTimeField('Event Start')
    end_date = models.DateTimeField('Event End')
    # venue = models.CharField('Venue', max_length=120) #luogo
    venue = models.ForeignKey(
        Venue, blank=True, null=True, on_delete=models.CASCADE)
    # manager = models.CharField('Manager Name', max_length=60, blank=True)
    # , on_delete=models.SETT_NULL
    manager = models.ForeignKey(
        User, blank=True, null=True, on_delete=models.CASCADE)
    description = models.TextField('Description', blank=True)
    attendees = models.ManyToManyField(myUser, blank=True)
    sport = models.ForeignKey(Sport, max_length=120,
                              blank=True, null=True, on_delete=models.CASCADE)

    objects = models.Manager()

    def __str__(self):
        return self.name

    def was_published_recently(self):
        now = timezone.now()
        return datetime.timedelta(days=3) - now <= self.start_date <= now
    
    def get_absolute_url(self):
        return reverse('event_detail', args=[self.id])
    

# First of all we need to create a subclass of HTMLCalendar which will generate the HTML for us.

class ContestCalendar(HTMLCalendar):

    def __init__(self, pContestEvents):
        super(ContestCalendar, self).__init__()
        self.contest_events = self.group_by_day(pContestEvents)
        
    def formatday(self, day, weekday):
        if day != 0:
            cssclass = self.cssclasses[weekday]
            if date.today() == date(self.year, self.month, day):
                cssclass += ' today'
            if day in self.contest_events:
                cssclass += ' filled'
                body = []
                for contest in self.contest_events[day]:
                    body.append('<a href="%s">' % contest.get_absolute_url()) # {'event': contest})
                    body.append(esc(contest.name))
                    body.append('</a><br/>')
                return self.day_cell(cssclass, '<div class="dayNumber">%d</div> %s' % (day, ''.join(body)))
            return self.day_cell(cssclass, '<div class="dayNumber">%d</div>' % day)
        return self.day_cell('noday', '&nbsp;')

    def formatmonth(self, year, month):
        self.year, self.month = year, month
        return super(ContestCalendar, self).formatmonth(year, month)
  

    def group_by_day(self, pContestEvents):
        field = lambda contest: contest.start_date.day
        return dict(
            [(day, list(items)) for day, items in groupby(pContestEvents, field)]
        )

   
    def day_cell(self, cssclass, body):
        return '<td class="%s">%s</td>' % (cssclass, body)
    
"""
if date() == date(self.year, self.month, day):
                cssclass += ' eventday'
                
Event.objects.all().filter(start_date__year=date)
"""