from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect

from .models import Event, Venue, myUser, Sport
from .forms import VenueForm, myUserForm, SportForm, EventForm

# calendar:
import calendar
from calendar import HTMLCalendar
from calendar import monthrange
from datetime import datetime,date,timedelta  # https://docs.python.org/3/library/datetime.html
# date = datetime.date
from .models import ContestCalendar  # Contest
from django.views import generic


def home(request):
    return render(request, 'events/home.html', {})

# ---------- view existing items in db

def all_venues(request):
    all_venue = Venue.objects.all()
    return render(request, 'events/venue_list.html', {
        "venue_list": all_venue})


def all_myUsers(request):
    all_myUser = myUser.objects.all()
    return render(request, 'events/myUser_list.html', {
        "myUser_list": all_myUser})


def all_sports(request):
    all_sport = Sport.objects.all()
    return render(request, 'events/sport_list.html', {
        "sport_list": all_sport})


def all_events(request):
    all_event = Event.objects.all()
    return render(request, 'events/event_list.html', {
        "event_list": all_event})

# ----------- ADD items to db

def add_venue(request):
    submitted = False
    if request.method == "POST":
        form = VenueForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/add_venue?submitted=True')
    else:
        form = VenueForm
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/add_venue.html', {
        'form': form, 'submitted': submitted
    })


def add_myUser(request):
    submitted = False
    if request.method == "POST":
        form = myUserForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/add_myUser?submitted=True')
    else:
        form = myUserForm
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/add_myUser.html', {
        'form': form, 'submitted': submitted
    })


def add_sport(request):
    submitted = False
    if request.method == "POST":
        form = SportForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/add_sport?submitted=True')
    else:
        form = SportForm
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/add_sport.html', {
        'form': form, 'submitted': submitted
    })


def add_event(request):
    submitted = False
    if request.method == "POST":
        form = EventForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/add_event?submitted=True')
    else:
        form = EventForm
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/add_event.html', {
        'form': form, 'submitted': submitted
    })

# --------- UPDATE and DELETE items

def update_venue(request, venue_id):
    submitted = False
    venue = Venue.objects.get(pk=venue_id)
    form = VenueForm(request.POST or None, instance=venue)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            # return redirect('list_venues')
            return HttpResponseRedirect('/update_venue/'+venue_id+'?submitted=True')
    else:
        # form = VenueForm()
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/update_venue.html', {
        'submitted': submitted, 'venue': venue, 'form': form
    })


def delete_venue(request, venue_id):
    venue = Venue.objects.get(pk=venue_id)
    venue.delete()
    return redirect('list_venues')


def update_myUser(request, myUser_id):
    submitted = False
    myuser = myUser.objects.get(pk=myUser_id)
    form = myUserForm(request.POST or None, instance=myuser)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            # return redirect('list_myUser')
            return HttpResponseRedirect('/update_myUser/'+myUser_id+'?submitted=True')
    else:
        # form = VenueForm()
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/update_myUser.html', {
        'submitted': submitted, 'myUser': myuser, 'form': form
    })


def delete_myUser(request, myUser_id):
    myuser = myUser.objects.get(pk=myUser_id)
    myuser.delete()
    return redirect('list_users')


def update_sport(request, sport_id):
    submitted = False
    sport = Sport.objects.get(pk=sport_id)
    form = SportForm(request.POST or None, instance=sport)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/update_sport/'+sport_id+'?submitted=True')
    else:
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/update_sport.html', {
        'submitted': submitted, 'sport': sport, 'form': form
    })


def delete_sport(request, sport_id):
    sport = Sport.objects.get(pk=sport_id)
    sport.delete()
    return redirect('list_sports')


def update_event(request, event_id):
    submitted = False
    event = Event.objects.get(pk=event_id)
    form = EventForm(request.POST or None, instance=event)
    if request.method == "POST":
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/update_event/'+event_id+'?submitted=True')
    else:
        if 'submitted' in request.GET:
            submitted = True

    return render(request, 'events/update_event.html', {
        'submitted': submitted, 'event': event, 'form': form
    })


def delete_event(request, event_id):
    event = Event.objects.get(pk=event_id)
    event.delete()
    return redirect('list_events')

#-------------

def search_event(request):
    if request.method == 'POST':
        event_searched = request.POST['event_searched']
        events = Event.object.filter(name__contains=event_searched)
        return render(request, 'event/search_event.html', {'event_searched': event_searched, 'events': events})
    else:
        return render(request, 'event/search_event.html', {})

class EventView(generic.DetailView): # display a detail page for a particular type of object
    model = Event
    template_name = 'events/event_detail.html'
 
# ---------- calendar : STATIC
"""
def calendar2(request, year = datetime.now().year, month = datetime.now().strftime('%B')):
    month = month.capitalize() # .title()
    # convert month from name to number
    month_number = datetime.strptime(month, '%B').month # not case sensitive !! But is better to capitalize it anyway
    month_number = int(month_number) # cast to be sure it will be an integer
    # month_number = list(calendar.month_name).index(month) # does not work with lower case initial months -> case sensitive
    cal = HTMLCalendar().formatmonth(year, month_number) # create a calendar associated to variable cal

    now = datetime.now()
    current_year = now.year
    time = now.strftime('%I:%M %p') # :%S seconds

    return render(request, 'events/calendar.html', {
        "year": year, "month": month, "month_number": month_number, "cal": cal,
        "current_year": current_year, "time": time,
    })
"""
""" import calendar     tc= calendar.HTMLCalendar(firstweekday=0)       print(tc.formatmonth(2016, 5))"""

# ---------- calendar : Dynamic:
"""We need to create an instance of this class from our view, passing in the list of objects from the specified month. 
Here I'm using two view functions, one of which calls the other. 
There is also a named_month function which returns the name of a month given the month number.
monthrange(pYear, pMonth) returns a tuple of the day numbers in the month, ie (1,31) for December."""


def named_month(pMonthNumber):
    """ Return the name of the month, given the month number"""
    return date(1900, pMonthNumber, 1).strftime('%B')


def calendar_now(request):
    """ Show calendar of events this month"""
    lToday = datetime.now()
    return calendar(request, lToday.year, lToday.month)


def calendar(request, pYear=datetime.now().year, pMonth=datetime.now().month):
    """ Show calendar of events for specified month and year """
    lYear = int(pYear)
    lMonth = int(pMonth)
    
    lCalendarFromMonth = datetime(lYear, lMonth, 1)
    lCalendarToMonth = datetime(lYear, lMonth, monthrange(lYear, lMonth)[1])

   # lContestEvents = Event.objects.filter(start_date=lCalendarFromMonth, end_date=lCalendarToMonth)
    lContestEvents = Event.objects.filter(start_date__gte=lCalendarFromMonth, end_date__lte=lCalendarToMonth)
    lCalendar = ContestCalendar(lContestEvents).formatmonth(lYear, lMonth)  # formatday(self, day, events) #
    lPreviousYear = lYear
    lPreviousMonth = lMonth - 1

    if lPreviousMonth == 0:
        lPreviousMonth = 12
        lPreviousYear = lYear - 1
    lNextYear = lYear
    lNextMonth = lMonth + 1

    if lNextMonth == 13:
        lNextMonth = 1
        lNextYear = lYear + 1
    lYearAfterThis = lYear + 1
    lYearBeforeThis = lYear - 1

    return render(request, 'events/calendar.html', {'Calendar': (lCalendar),
                                                    'Month': lMonth,
                                                    'MonthName': named_month(lMonth),
                                                    'Year': lYear,
                                                    'PreviousMonth': lPreviousMonth,
                                                    'PreviousMonthName': named_month(lPreviousMonth),
                                                    'PreviousYear': lPreviousYear,
                                                    'NextMonth': lNextMonth,
                                                    'NextMonthName': named_month(lNextMonth),
                                                    'NextYear': lNextYear,
                                                    'YearBeforeThis': lYearBeforeThis,
                                                    'YearAfterThis': lYearAfterThis,
                                                    })


