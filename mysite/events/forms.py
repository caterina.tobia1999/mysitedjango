from django import forms
from django.forms import ModelForm
# from models import *model_name*
from .models import Venue
from .models import myUser
from .models import Sport
from .models import Event


# venue form
class VenueForm(ModelForm):
    class Meta: #django define things
        model = Venue
        # fields = "__all__"
        fields = ('name', 'city', 'address', 'zip_code', 'phone', 'web', 'email_address')
        labels = {
            'name': 'Venue Name',
            'city': 'City',
            'address': 'Address',
            'zip_code': 'Zip_code',
            'phone': 'Phone',
            'web': 'Web link',
            'email_address': 'Email Address'
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Infinity boulder'}),
            'city': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Mattarello (TN)'}),
            'address': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Località Le Basse, 25'}),
            'zip_code': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '38122'}),
            'phone': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '3457767114'}),
            'web': forms.URLInput(attrs={'class': 'form-control', 'placeholder': 'http://infinityboulder.it'}),
            'email_address': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'infinityboulder@gmail.com'})
        }

# myUser form
class myUserForm(ModelForm):
    class Meta: #django define things
        model = myUser
        # fields = "__all__"
        fields = ('first_name', 'last_name', 'birth_date', 'email')
        labels = {
            'first_name': 'First Name',
            'last_name': 'Last Name',
            'birth_date': 'Birth Date: YYYY-MM-DD',
            'email_address': 'Email Address'
        }
        widgets = {
            'first_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Anna'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Veronese'}),
            'birth_date': forms.DateInput(attrs={'class': 'form-control', 'placeholder': '1999-01-25'}),
            'email_address': forms.EmailInput(attrs={'class': 'form-control', 'placeholder': 'AnnaV@gmail.com'})
        }

class SportForm(ModelForm):
    class Meta: #django define things
        model = Sport
        # fields = "__all__"
        fields = ('name', 'description')
        labels = {
            'name': 'Sport Name',
            'description': 'Sport Description'
        }
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'climbing'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': '...'})
        }

class EventForm(ModelForm):
    class Meta: #django define things
        model = Event
        # fields = "__all__"
        fields = ('name', 'venue', 'start_date', 'end_date', 'description', 'manager', 'attendees', 'sport')
        labels = {
            'name': 'Event Name',
            'venue': 'Select Venue',
            'start_date': 'Start Date and Time: YYYY-MM-DD HH:MM:SS',
            'end_date': 'End Date and Time: YYYY-MM-DD HH:MM:SS',
            'description': 'Description (optional)',
            'manager': 'Select Guide (optional)',
            'attendees': 'Select Multiple Attendees (optional)',
            'sport': 'Select Sport'
        }
        # CHOICES = (('Option 1', 'Option 1'), ('Option 2', 'Option 2'),) # Venue.objects.all()
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Indoor boulder climbing'}),
            # 'venue': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Infinity Boulder: Località Le Basse, 25'}),
            #'venue': forms.ChoiceField(choices=CHOICES),
            'venue': forms.Select(attrs={'class': 'regDropDown'}),
            'start_date': forms.DateTimeInput(attrs={'class': 'form-control', 'placeholder': '2023-10-25 14:30:59'}),
            'end_date': forms.DateTimeInput(attrs={'class': 'form-control', 'placeholder': '2023-10-25 17:30:59'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'placeholder': '...'}),
            #'manager': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '...'}),
            'manager': forms.Select(attrs={'class': 'regDropDown'}),
            #'attendees': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '...'}),
            'attendees': forms.SelectMultiple(attrs={'class': 'regDropDown'}),
            #'sport': forms.TextInput(attrs={'class': 'form-control', 'placeholder': '...'})
            'sport': forms.Select(attrs={'class': 'regDropDown'})
        }