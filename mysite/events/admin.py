from django.contrib import admin

from .models import Venue, myUser, Event, Sport

# admin.site.register(Venue)
@admin.register(Venue)
class VenueAdmin(admin.ModelAdmin):
    fields = ('name', ('city', 'address', 'zip_code'), 'phone', 'web', 'email_address')
    list_display = ('name', 'address')
    ordering = ('name',)
    search_fields = ('name', 'address')
    
# admin.site.register(Event)
@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    fields = (('name', 'venue'), ('start_date', 'end_date'),
              'description', 'manager', 'attendees', 'sport')
    list_display = ("name", 'start_date', 'end_date', 'venue')
    list_filter = ('start_date', 'end_date', 'venue')
    ordering = ('-start_date',)
    # search_fields = ('name', 'address')

# admin.site.register(myUser)
@admin.register(myUser)
class myUserAdmin(admin.ModelAdmin):
    fields = (('first_name', 'last_name'), 'email', 'birth_date')
    list_display = ("first_name", 'last_name')
    list_filter = ('first_name', 'last_name', 'birth_date')
    ordering = ('last_name',)

# admin.site.register(Sport)
@admin.register(Sport)
class SportAdmin(admin.ModelAdmin):
    fields = ('name', 'description')
    ordering = ('name',)